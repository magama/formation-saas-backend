﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Formation.Account
{
    public static class AccountStream
    {
        public static string GetName(Guid accountId)
        {
            return $"account_{accountId}";
        }
    }
}

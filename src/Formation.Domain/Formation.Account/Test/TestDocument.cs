﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Formation.MongoDb;

using MongoDB.Driver;

namespace Formation.Account.Test
{
    public class TestDocument: MongoDocument
    {
        public Guid EventId { get; set; }

        public static async Task CreateIndexesAsync(IMongoCollection<TestDocument> collection)
        {
            var uniqueIndex = Builders<TestDocument>.IndexKeys.Ascending(x => x.EventId);

            await collection.Indexes.CreateOneAsync(uniqueIndex, new CreateIndexOptions { Unique = true });
        }
    }
}
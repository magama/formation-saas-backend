﻿using System;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Formation.Account.Events;
using Formation.Account.Projection;
using Formation.Common.EventSourcing;
using Formation.MongoDb;

namespace Formation.Account.Test
{
    public sealed class TestProjectionHandler :
        IEventHandler<TestEvent>
    {
        private readonly MongoRepository<TestDocument> _repository;

        public TestProjectionHandler(MongoRepository<TestDocument> repository)
        {
            this._repository = repository;
        }

        public async Task HandleAsync(SubscribedEvent<TestEvent> subscribed, CancellationToken cancellationToken = default(CancellationToken))
        {
            TestEvent @event = subscribed.Event;

            await this._repository.InsertOneAsync(
                new TestDocument
                {
                    EventId = @event.Id
                });
        }
    }
}
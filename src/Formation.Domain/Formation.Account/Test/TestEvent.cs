﻿using System;
using System.Linq;
using System.Text;

using Formation.Common.EventSourcing;

namespace Formation.Account.Test
{
    public sealed class TestEvent : DomainEvent
    {
        public TestEvent(Guid id)
        {
            this.Id = id;
        }

        public Guid Id { get; }
    }
}
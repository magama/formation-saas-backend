﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;

using Formation.Account.Events;

namespace Formation.Account
{
    public sealed class AccountAggregateStateSolution
    {
        public bool IsCreated { get; private set; }

        public Guid AccountId { get; private set; }

        public void Apply(AccountCreatedEvent @event)
        {
            this.AccountId = @event.AccountId;
            this.IsCreated = true;
        }
       
    }
}
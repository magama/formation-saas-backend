﻿using System;
using System.Linq;
using System.Text;

using Formation.Account.Events;
using Formation.EventStore.Aggregate;

namespace Formation.Account
{
    public sealed class AccountAggregateSolution : AggregateRoot<AccountAggregateStateSolution>
    {
        public AccountAggregateSolution()
            : base(new AccountAggregateStateSolution())
        {
        }

        public AccountAggregateSolution(AccountAggregateStateSolution stateSolution)
            : base(stateSolution)
        {
        }

        public OperationResult CreateAccount(Guid accountId, string accountName)
        {
            if (accountId == Guid.Empty)
            {
                return OperationResult.Failure("InvalidId");
            }

            if (this.StateSolution.IsCreated)
            {
                return OperationResult.Failure("AlreadyExist");
            }

            this.ApplyEvent(new AccountCreatedEvent(accountId, accountName));

            return OperationResult.Success;
        }
    }
}
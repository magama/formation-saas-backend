﻿using System;

using Formation.Account.Events;
using Formation.EventStore.Aggregate;

namespace Formation.Account
{
    public sealed class AccountAggregate : AggregateRoot<AccountAggregateState>
    {
        public AccountAggregate()
            : base(new AccountAggregateState())
        {
        }

        public AccountAggregate(AccountAggregateState stateSolution)
            : base(stateSolution)
        {
        }
    }
}
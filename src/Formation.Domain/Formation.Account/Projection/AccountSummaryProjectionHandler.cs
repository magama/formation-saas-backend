﻿using System.Threading;
using System.Threading.Tasks;

using Formation.Account.Events;
using Formation.Common.EventSourcing;
using Formation.MongoDb;

namespace Formation.Account.Projection
{
    public sealed class AccountSummaryProjectionHandler :
        IEventHandler<AccountCreatedEvent>
    {
        private readonly MongoRepository<AccountSummaryDocument> _repository;

        public AccountSummaryProjectionHandler(MongoRepository<AccountSummaryDocument> repository)
        {
            this._repository = repository;
        }

        public async Task HandleAsync(SubscribedEvent<AccountCreatedEvent> subscribed, CancellationToken cancellationToken = default(CancellationToken))
        {
            var @event = subscribed.Event;

            await this._repository.InsertOneAsync(
                new AccountSummaryDocument
                {
                    AccountId = @event.AccountId,
                    AccountName = @event.Name,
                    Total = 0,
                });
        }
    }
}
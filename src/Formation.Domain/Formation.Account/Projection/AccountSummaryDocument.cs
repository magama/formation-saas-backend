﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Formation.MongoDb;

using MongoDB.Driver;

namespace Formation.Account.Projection
{
    public class AccountSummaryDocument: MongoDocument
    {
        public Guid AccountId { get; set; }

        public string AccountName { get; set; }

        public decimal Total { get; set; }

        public static async Task CreateIndexesAsync(IMongoCollection<AccountSummaryDocument> collection)
        {
            var uniqueIndex = Builders<AccountSummaryDocument>.IndexKeys.Ascending(x => x.AccountId);

            await collection.Indexes.CreateOneAsync(uniqueIndex, new CreateIndexOptions { Unique = true });
        }
    }
}
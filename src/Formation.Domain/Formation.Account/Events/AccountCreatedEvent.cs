﻿using System;
using System.Linq;
using System.Text;

using Formation.Common.EventSourcing;

namespace Formation.Account.Events
{
    public sealed class AccountCreatedEvent : DomainEvent
    {
        public AccountCreatedEvent(Guid accountId, string name)
        {
            this.AccountId = accountId;
            this.Name = name;
        }

        public Guid AccountId { get; }

        public string Name { get; }
    }
}
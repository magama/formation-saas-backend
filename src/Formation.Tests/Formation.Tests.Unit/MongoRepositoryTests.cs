using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Formation.MongoDb;
using Formation.Tests.Unit.Fixture;

using MongoDB.Driver;

using Xunit;

namespace Formation.Tests.Unit
{
    [Collection(XUnitCollections.MongoDb)]
    public class MongoRepositoryTests : IClassFixture<AppSettingsFixture>
    {
        private readonly MongoRepository<CompositeDocument> _mongoRepository;

        public MongoRepositoryTests(MongoDbFixture mongoDbFixture)
        {
            this._mongoRepository = mongoDbFixture.CreateMongoRepository<CompositeDocument>(CompositeDocument.CreateIndexes);
        }

        [Fact]
        public async Task Can_Insert()
        {
            // Arrange
            var sitecollectionId = Guid.NewGuid();
            var siteId = Guid.NewGuid();
            var data = "cosmopolitain";
            var document = new CompositeDocument()
            {
                SiteCollectionId = sitecollectionId,
                SiteId = siteId,
                Data = data,
            };

            // Act
            await this._mongoRepository.InsertOneAsync(document);

            // Assert
            var documents = await this.GetAllDocuments();
            var single = Assert.Single(documents);
            Assert.Equal(sitecollectionId, single.SiteCollectionId);
            Assert.Equal(siteId, single.SiteId);
            Assert.Equal(data, single.Data);
        }

        [Fact]
        public async Task Given_Document_With_Same_Index_Exist_When_Insert_Then_Exception()
        {
            // Given
            var sitecollectionId = Guid.NewGuid();
            var siteId = Guid.NewGuid();
            var existingDocument = new CompositeDocument()
            {
                SiteCollectionId = sitecollectionId,
                SiteId = siteId,
            };

            await this._mongoRepository.InsertOneAsync(existingDocument);

            var duplicateDocument = new CompositeDocument()
            {
                SiteCollectionId = sitecollectionId,
                SiteId = siteId,
            };

            // When-Then
            await Assert.ThrowsAsync<MongoWriteException>(() => this._mongoRepository.InsertOneAsync(duplicateDocument));
        }

        [Fact]
        public async Task Given_One_Match_When_FindOneAndUpdate_Then_Update_One()
        {
            // Given
            var sitecollectionId = Guid.NewGuid();
            var siteId = Guid.NewGuid();
            var data = "margarita";
            var updateData = "daiquiri";

            var document = new CompositeDocument()
            {
                SiteCollectionId = sitecollectionId,
                SiteId = siteId,
                Data = data,
            };

            var otherDocument = new CompositeDocument
            {
                SiteCollectionId = Guid.NewGuid(),
                SiteId = Guid.NewGuid(),
                Data = data,
            };

            await this._mongoRepository.InsertOneAsync(document);
            await this._mongoRepository.InsertOneAsync(otherDocument);

            // When
            var filter = Builders<CompositeDocument>.Filter.Eq(x => x.SiteCollectionId, sitecollectionId);
            var update = Builders<CompositeDocument>.Update
                .Set(x => x.Data, updateData);
            await this._mongoRepository.UpdateOneAsync(filter, update);

            // Then
            var result = await this._mongoRepository.FindOneAsync(filter);

            Assert.NotNull(result);
            Assert.Equal(updateData, result.Data);
        }

        [Fact]
        public async Task Given_Multiple_Match_When_FindOneAndUpdate_Then_Update_One_Then_Error()
        {
            // Given
            var sitecollectionId = Guid.NewGuid();
            var siteId = Guid.NewGuid();
            var data = "quebec royal";
            var document = new CompositeDocument
            {
                SiteCollectionId = sitecollectionId,
                SiteId = siteId,
                Data = data,
            };

            var otherDocument = new CompositeDocument()
            {
                SiteCollectionId = sitecollectionId,
                SiteId = Guid.NewGuid(),
                Data = data,
            };

            await this._mongoRepository.InsertOneAsync(document);
            await this._mongoRepository.InsertOneAsync(otherDocument);

            var filter = Builders<CompositeDocument>.Filter.Eq(x => x.SiteCollectionId, sitecollectionId);
            var update = Builders<CompositeDocument>.Update
                .Set(x => x.Data, "bbb");

            // When-Then
            await Assert.ThrowsAsync<MongoDuplicateDocumentException>(() => this._mongoRepository.UpdateOneAsync(filter, update));
        }

        [Fact]
        public async Task Given_Multiple_Match_When_FindOneAndUpsert_Then_Update_One_Then_Error()
        {
            // Given
            var sitecollectionId = Guid.NewGuid();
            var siteId = Guid.NewGuid();
            var data = "old fashioned";
            var document = new CompositeDocument
            {
                SiteCollectionId = sitecollectionId,
                SiteId = siteId,
                Data = data,
            };

            var otherDocument = new CompositeDocument()
            {
                SiteCollectionId = sitecollectionId,
                SiteId = Guid.NewGuid(),
                Data = data,
            };

            await this._mongoRepository.InsertOneAsync(document);
            await this._mongoRepository.InsertOneAsync(otherDocument);

            var filter = Builders<CompositeDocument>.Filter.Eq(x => x.SiteCollectionId, sitecollectionId);
            var update = Builders<CompositeDocument>.Update
                .Set(x => x.Data, "bbb");

            // When-Then
            await Assert.ThrowsAsync<MongoDuplicateDocumentException>(() => this._mongoRepository.UpsertOneAsync(filter, update));
        }

        [Fact]
        public async Task Given_Many_Documents_When_Find_Async_Return_Filtered_Documents()
        {
            // Given
            var sitecollectionId = Guid.NewGuid();
            var dataToFilter = "old fashioned";

            var filterDocument1 = new CompositeDocument
            {
                SiteCollectionId = sitecollectionId,
                SiteId = Guid.NewGuid(),
                Data = dataToFilter,
            };

            var filterDocument2 = new CompositeDocument
            {
                SiteCollectionId = sitecollectionId,
                SiteId = Guid.NewGuid(),
                Data = dataToFilter,
            };

            var otherDocument = new CompositeDocument()
            {
                SiteCollectionId = sitecollectionId,
                SiteId = Guid.NewGuid(),
                Data = "pinacolada",
            };

            await this._mongoRepository.InsertOneAsync(filterDocument1);
            await this._mongoRepository.InsertOneAsync(filterDocument2);
            await this._mongoRepository.InsertOneAsync(otherDocument);

            var filter = Builders<CompositeDocument>.Filter.Eq(x => x.Data, dataToFilter);

            // When
            var results = (await this._mongoRepository.FindAsync(filter)).ToList();

            // Then
            Assert.Equal(2, results.Count);
            Assert.Contains(results, x => x.SiteId == filterDocument1.SiteId);
            Assert.Contains(results, x => x.SiteId == filterDocument2.SiteId);
            Assert.DoesNotContain(results, x => x.SiteId == otherDocument.SiteId);
        }

        [Fact]
        public async Task Given_Many_Documents_When_Find_All_Async_Return_All()
        {
            // Given
            var sitecollectionId = Guid.NewGuid();
            var dataToFilter = "old fashioned";

            var filterDocument1 = new CompositeDocument
            {
                SiteCollectionId = sitecollectionId,
                SiteId = Guid.NewGuid(),
                Data = dataToFilter,
            };

            var filterDocument2 = new CompositeDocument
            {
                SiteCollectionId = sitecollectionId,
                SiteId = Guid.NewGuid(),
                Data = dataToFilter,
            };

            var otherDocument = new CompositeDocument()
            {
                SiteCollectionId = sitecollectionId,
                SiteId = Guid.NewGuid(),
                Data = "pinacolada",
            };

            await this._mongoRepository.InsertOneAsync(filterDocument1);
            await this._mongoRepository.InsertOneAsync(filterDocument2);
            await this._mongoRepository.InsertOneAsync(otherDocument);

            // When
            var results = (await this._mongoRepository.FindAllAsync()).ToList();

            // Then
            Assert.Equal(3, results.Count);
            Assert.Contains(results, x => x.SiteId == filterDocument1.SiteId);
            Assert.Contains(results, x => x.SiteId == filterDocument2.SiteId);
            Assert.Contains(results, x => x.SiteId == otherDocument.SiteId);
        }

        [Fact]
        public async Task Given_Matching_Document_When_Any_Return_True()
        {
            // Given
            var sitecollectionId = Guid.NewGuid();
            var dataToFilter = "old fashioned";

            var filterDocument1 = new CompositeDocument
            {
                SiteCollectionId = sitecollectionId,
                SiteId = Guid.NewGuid(),
                Data = dataToFilter,
            };

            var otherDocument = new CompositeDocument()
            {
                SiteCollectionId = sitecollectionId,
                SiteId = Guid.NewGuid(),
                Data = "pinacolada",
            };

            await this._mongoRepository.InsertOneAsync(otherDocument);
            await this._mongoRepository.InsertOneAsync(filterDocument1);

            var filter = Builders<CompositeDocument>.Filter.Eq(x => x.Data, dataToFilter);

            // When
            var result = await this._mongoRepository.AnyAsync(filter);

            // Then
            Assert.True(result);
        }

        [Fact]
        public async Task Given_No_Matching_Document_When_Any_Return_False()
        {
            // Given
            var sitecollectionId = Guid.NewGuid();
            var dataToFilter = "old fashioned";

            var otherDocument = new CompositeDocument()
            {
                SiteCollectionId = sitecollectionId,
                SiteId = Guid.NewGuid(),
                Data = "pinacolada",
            };

            await this._mongoRepository.InsertOneAsync(otherDocument);

            var filter = Builders<CompositeDocument>.Filter.Eq(x => x.Data, dataToFilter);

            // When
            var result = await this._mongoRepository.AnyAsync(filter);

            // Then
            Assert.False(result);
        }

        [Fact]
        public async Task Given_All_Document_By_Site_Collection_When_All_Return_True()
        {
            // Given
            var sitecollectionId = Guid.NewGuid();
            var dataToFilter = "old fashioned";

            var filterDocument1 = new CompositeDocument
            {
                SiteCollectionId = sitecollectionId,
                SiteId = Guid.NewGuid(),
                Data = dataToFilter,
            };

            var filterDocument2 = new CompositeDocument
            {
                SiteCollectionId = sitecollectionId,
                SiteId = Guid.NewGuid(),
                Data = dataToFilter,
            };

            var otherDocument = new CompositeDocument
            {
                SiteCollectionId = Guid.NewGuid(),
                SiteId = Guid.NewGuid(),
                Data = "daiquiri",
            };

            await this._mongoRepository.InsertOneAsync(filterDocument1);
            await this._mongoRepository.InsertOneAsync(filterDocument2);
            await this._mongoRepository.InsertOneAsync(otherDocument);

            // When
            var filter = Builders<CompositeDocument>.Filter.Eq(x => x.SiteCollectionId, sitecollectionId);
            var predicate = Builders<CompositeDocument>.Filter.Eq(x => x.Data, dataToFilter);

            var result = await this._mongoRepository.AllAsync(filter, predicate);

            // Then
            Assert.True(result);
        }

        [Fact]
        public async Task When_Select_Disctincs_Then_Return_Distincs()
        {
            // Given
            var siteA = Guid.NewGuid();
            var siteB = Guid.NewGuid();
            var dataToFilter = "old fashioned";

            var document1 = new CompositeDocument
            {
                SiteCollectionId = Guid.NewGuid(),
                SiteId = siteA,
                Data = dataToFilter,
            };

            var document2 = new CompositeDocument
            {
                SiteCollectionId = Guid.NewGuid(),
                SiteId = siteA,
                Data = dataToFilter,
            };

            var document3 = new CompositeDocument
            {
                SiteCollectionId = Guid.NewGuid(),
                SiteId = siteB,
                Data = dataToFilter,
            };

            var otherDocument = new CompositeDocument
            {
                SiteCollectionId = Guid.NewGuid(),
                SiteId = Guid.NewGuid(),
                Data = "daiquiri",
            };

            await this._mongoRepository.InsertOneAsync(document1);
            await this._mongoRepository.InsertOneAsync(document2);
            await this._mongoRepository.InsertOneAsync(document3);
            await this._mongoRepository.InsertOneAsync(otherDocument);

            // When
            var filter = Builders<CompositeDocument>.Filter.Eq(x => x.Data, dataToFilter);

            var result = await this._mongoRepository.DistinctAsync<Guid>(filter, nameof(CompositeDocument.SiteId));

            // Then
            Assert.Equal(2, result.Count);
            Assert.Contains(siteA, result);
            Assert.Contains(siteB, result);
        }

        [Fact]
        public async Task Given_Not_All_Document_When_All_Return_False()
        {
            // Given
            var sitecollectionId = Guid.NewGuid();
            var dataToFilter = "old fashioned";

            var filterDocument1 = new CompositeDocument
            {
                SiteCollectionId = sitecollectionId,
                SiteId = Guid.NewGuid(),
                Data = dataToFilter,
            };

            var otherDocument = new CompositeDocument
            {
                SiteCollectionId = sitecollectionId,
                SiteId = Guid.NewGuid(),
                Data = "daiquiri",
            };

            await this._mongoRepository.InsertOneAsync(filterDocument1);
            await this._mongoRepository.InsertOneAsync(otherDocument);

            // When
            var filter = Builders<CompositeDocument>.Filter.Eq(x => x.SiteCollectionId, sitecollectionId);
            var predicate = Builders<CompositeDocument>.Filter.Eq(x => x.Data, dataToFilter);

            var result = await this._mongoRepository.AllAsync(filter, predicate);

            // Then
            Assert.False(result);
        }

        [Fact]
        public async Task Given_Query_Options_Limit_Then_Return_Right_Amount_Document()
        {
            // Given
            var sitecollectionId = Guid.NewGuid();
            var limit = 2;

            var document1 = new CompositeDocument
            {
                SiteCollectionId = sitecollectionId,
                SiteId = Guid.NewGuid(),
                Data = "old fashioned",
            };

            var document2 = new CompositeDocument
            {
                SiteCollectionId = sitecollectionId,
                SiteId = Guid.NewGuid(),
                Data = "daiquiri",
            };

            var document3 = new CompositeDocument
            {
                SiteCollectionId = sitecollectionId,
                SiteId = Guid.NewGuid(),
                Data = "margarita",
            };

            await this._mongoRepository.InsertOneAsync(document1);
            await this._mongoRepository.InsertOneAsync(document2);
            await this._mongoRepository.InsertOneAsync(document3);

            // When
            var filter = Builders<CompositeDocument>.Filter.Eq(x => x.SiteCollectionId, sitecollectionId);

            var options = new FindOptions<CompositeDocument, CompositeDocument>
            {
                Limit = limit,
            };

            var result = await this._mongoRepository.FindAsync(filter, options);

            // Then
            Assert.Equal(limit, result.Count());
        }

        [Fact]
        public async Task Given_Query_Options_Sort_Descending_Then_Return_Sorted_Documents()
        {
            // Given
            var sitecollectionId = Guid.NewGuid();
            var firstName = "a_old fashioned";
            var secondName = "b_daiquiri";
            var thirdName = "c_margarita";

            var document1 = new CompositeDocument
            {
                SiteCollectionId = sitecollectionId,
                SiteId = Guid.NewGuid(),
                Data = firstName,
            };

            var document2 = new CompositeDocument
            {
                SiteCollectionId = sitecollectionId,
                SiteId = Guid.NewGuid(),
                Data = secondName,
            };

            var document3 = new CompositeDocument
            {
                SiteCollectionId = sitecollectionId,
                SiteId = Guid.NewGuid(),
                Data = thirdName,
            };

            // Do not add in order to validate sorting
            await this._mongoRepository.InsertOneAsync(document3);
            await this._mongoRepository.InsertOneAsync(document1);
            await this._mongoRepository.InsertOneAsync(document2);

            // When
            var filter = Builders<CompositeDocument>.Filter.Eq(x => x.SiteCollectionId, sitecollectionId);

            var options = new FindOptions<CompositeDocument, CompositeDocument>
            {
                Sort = Builders<CompositeDocument>.Sort.Ascending(nameof(CompositeDocument.Data)),
            };

            var result = await this._mongoRepository.FindAsync(filter, options);
            var dataListInOrder = result.Select(x => x.Data).ToList();

            // Then
            // Validate order
            Assert.Equal(firstName, dataListInOrder[0]);
            Assert.Equal(secondName, dataListInOrder[1]);
            Assert.Equal(thirdName, dataListInOrder[2]);
        }

        [Fact]
        public async Task Given_Query_Options_Sort_Descending_And_Limit_Then_Return_Sorted_Documents()
        {
            // Given
            var sitecollectionId = Guid.NewGuid();
            var firstName = "a_old fashioned";
            var secondName = "b_daiquiri";
            var thirdName = "c_margarita";
            var limit = 2;

            var document1 = new CompositeDocument
            {
                SiteCollectionId = sitecollectionId,
                SiteId = Guid.NewGuid(),
                Data = firstName,
            };

            var document2 = new CompositeDocument
            {
                SiteCollectionId = sitecollectionId,
                SiteId = Guid.NewGuid(),
                Data = secondName,
            };

            var document3 = new CompositeDocument
            {
                SiteCollectionId = sitecollectionId,
                SiteId = Guid.NewGuid(),
                Data = thirdName,
            };

            // Do not add in order to validate sorting
            await this._mongoRepository.InsertOneAsync(document3);
            await this._mongoRepository.InsertOneAsync(document1);
            await this._mongoRepository.InsertOneAsync(document2);

            // When
            var filter = Builders<CompositeDocument>.Filter.Eq(x => x.SiteCollectionId, sitecollectionId);

            var options = new FindOptions<CompositeDocument, CompositeDocument>
            {
                Limit = limit,
                Sort = Builders<CompositeDocument>.Sort.Ascending(nameof(CompositeDocument.Data)),
            };

            var result = await this._mongoRepository.FindAsync(filter, options);
            var dataListInOrder = result.Select(x => x.Data).ToList();

            // Then
            Assert.Equal(limit, dataListInOrder.Count);

            // Validate order
            Assert.Equal(firstName, dataListInOrder[0]);
            Assert.Equal(secondName, dataListInOrder[1]);
        }

        private async Task<IList<CompositeDocument>> GetAllDocuments()
        {
            var filter = Builders<CompositeDocument>.Filter.Empty;
            var results = await this._mongoRepository.FindAsync(filter);
            return results.ToList();
        }

        private class CompositeDocument : MongoDocument
        {
            public Guid SiteCollectionId { get; set; }

            public Guid SiteId { get; set; }

            public string Data { get; set; }

            public static async Task CreateIndexes(IMongoCollection<CompositeDocument> collection)
            {
                var index = Builders<CompositeDocument>.IndexKeys.Combine(
                    Builders<CompositeDocument>.IndexKeys.Ascending(x => x.SiteCollectionId),
                    Builders<CompositeDocument>.IndexKeys.Ascending(x => x.SiteId));
                await collection.Indexes.CreateOneAsync(index, new CreateIndexOptions { Unique = true });
            }
        }
    }
}

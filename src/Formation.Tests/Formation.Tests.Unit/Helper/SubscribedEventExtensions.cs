﻿using System;
using System.Linq;
using System.Text;

using Formation.Common.EventSourcing;

namespace Formation.Tests.Unit.Helper
{
    public static class SubscribedEventExtensions
    {
        public static SubscribedEvent<TEvent> ToSubscribed<TEvent>(this TEvent @event)
            where TEvent : DomainEvent
        {
            return new SubscribedEvent<TEvent>(Guid.Empty, @event, null, 0);
        }

        public static SubscribedEvent<TEvent> ToSubscribed<TEvent>(this TEvent @event, Guid eventId)
            where TEvent : DomainEvent
        {
            return new SubscribedEvent<TEvent>(eventId, @event, null, 0);
        }
    }
}
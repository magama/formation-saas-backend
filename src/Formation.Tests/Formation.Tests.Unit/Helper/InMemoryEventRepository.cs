﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Formation.Common.EventSourcing;
using Formation.EventStore;
using Formation.EventStore.Plumbing;

namespace Formation.Tests.Unit.Helper
{
    public class InMemoryEventRepository : IEventRepository
    {
        private readonly Dictionary<string, ImmutableList<DomainEvent>> _db = new Dictionary<string, ImmutableList<DomainEvent>>();
        private readonly Dictionary<string, ImmutableList<DomainEvent>> _operationCheckpoints = new Dictionary<string, ImmutableList<DomainEvent>>();
        private ImmutableList<(string StreamId, DomainEvent Event)> _allCheckpoint = ImmutableList<(string StreamId, DomainEvent Event)>.Empty;

        public ImmutableList<(string StreamId, DomainEvent Event)> All { get; private set; } = ImmutableList<(string, DomainEvent)>.Empty;

        public IList<DomainEvent> StreamOperationEvents(string streamId) => this._db[streamId].Skip(this._operationCheckpoints[streamId].Count).ToList();

        public IList<DomainEvent> AllOperationEvents() => this.All.Skip(this._allCheckpoint.Count).Select(t => t.Event).ToList();

        public IList<DomainEvent> Stream(string streamId) => this._db[streamId].ToList();

        public bool IsStreamEmpty(string streamId) => !this._db.ContainsKey(streamId);

        public Task<IReadOnlyList<CommitedEvent>> GetStreamEventsAsync(string streamId, long fromVersionInclusive, CancellationToken cancellationToken = default(CancellationToken))
        {
            if (!this._db.ContainsKey(streamId))
            {
                IReadOnlyList<CommitedEvent> commitedEvents = new List<CommitedEvent>().AsReadOnly();
                return Task.FromResult(commitedEvents);
            }

            var events = this._db[streamId].Skip((int)fromVersionInclusive);

            IReadOnlyList<CommitedEvent> result = events.Select(
                (t, i) =>
                    new CommitedEvent(Guid.Empty, streamId, fromVersionInclusive + i, t)).ToList().AsReadOnly();

            return Task.FromResult(result);
        }

        public Task<(long NextEventNumber, bool IsEndOfStream, IReadOnlyList<CommitedEvent> Events)> ReadStreamEventsForwardAsync(
            string streamId,
            long fromVersionInclusive,
            int count = 128,
            bool resolveLinkTo = false,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            if (!this._db.ContainsKey(streamId))
            {
                IReadOnlyList<CommitedEvent> commitedEvents = new List<CommitedEvent>().AsReadOnly();
                return Task.FromResult((0L, true, commitedEvents));
            }

            var events = this._db[streamId].Skip((int)fromVersionInclusive);

            IReadOnlyList<CommitedEvent> result = events.Skip((int)fromVersionInclusive).Take(count).Select(
                (t, i) =>
                    new CommitedEvent(Guid.Empty, streamId, fromVersionInclusive + i, t)).ToList().AsReadOnly();

            return Task.FromResult(((long)result.Count, false, result));
        }

        public void CheckPointOperation()
        {
            this._operationCheckpoints.Clear();
            this._allCheckpoint = this.All;
            foreach (var item in this._db)
            {
                this._operationCheckpoints.Add(item.Key, item.Value);
            }
        }

        public Task AppendAsync(string streamId, IEnumerable<DomainEvent> events)
        {
            if (!this._db.ContainsKey(streamId))
            {
                this._db.Add(streamId, ImmutableList<DomainEvent>.Empty);
            }

            this._db[streamId] = this._db[streamId].AddRange(events.Select(e => (e)));
            this.All = this.All.AddRange(events.Select(e => (streamId, e)));

            return Task.CompletedTask;
        }

        public Task SaveAsync(string streamId, IEnumerable<UncommitedEvent> events)
        {
            return this.AppendAsync(streamId, events.Select(x => x.DomainEvent));
        }

        public Task AppendSingleAsync<TDomainEvent>(string streamId, TDomainEvent @event)
            where TDomainEvent : DomainEvent
        {
            return this.AppendAsync(streamId, new DomainEvent[] { @event });
        }
    }
}
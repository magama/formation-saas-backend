﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using Formation.Tests.Unit.Fixture;

using Newtonsoft.Json;

using Xunit;

namespace Formation.Tests.Unit.Account
{
    public class AccountControllerTests : IClassFixture<AppSettingsFixture>, IClassFixture<WebHostFixture>
    {
        private readonly HttpClient _httpClient;

        public AccountControllerTests(WebHostFixture fixture)
        {
            this._httpClient = fixture.Client;
        }

        [Fact]
        public async Task Test()
        {
            // Arrange
            var request = new HttpRequestMessage(HttpMethod.Post, "accounts/bendoyon");

            // Act
            var response = await this._httpClient.SendAsync(request);

            // Assert
            var content = await response.Content.ReadAsStringAsync();

            Assert.Equal(HttpStatusCode.OK, response.StatusCode);

            var id = JsonConvert.DeserializeObject<Guid>(content);

            Assert.NotEqual(Guid.Empty, id);
        }
    }
}

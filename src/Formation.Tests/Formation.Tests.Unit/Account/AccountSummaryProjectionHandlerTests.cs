﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Formation.Account.Events;
using Formation.Account.Projection;
using Formation.MongoDb;
using Formation.Tests.Unit.Fixture;
using Formation.Tests.Unit.Helper;

using Xunit;

namespace Formation.Tests.Unit.Account
{
    [Collection(XUnitCollections.MongoDb)]
    public class AccountSummaryProjectionHandlerTests
    {
        private static readonly Guid DefaultAccountId = Guid.NewGuid();
        private static readonly string DefaultName = "cosmo";

        private readonly MongoRepository<AccountSummaryDocument> _mongoDbRepository;
        private readonly AccountSummaryProjectionHandler _handler;

        public AccountSummaryProjectionHandlerTests(MongoDbFixture mongoDbFixture)
        {
            this._mongoDbRepository = mongoDbFixture.CreateMongoRepository<AccountSummaryDocument>(AccountSummaryDocument.CreateIndexesAsync);
            this._handler = new AccountSummaryProjectionHandler(this._mongoDbRepository);
        }
    }
}
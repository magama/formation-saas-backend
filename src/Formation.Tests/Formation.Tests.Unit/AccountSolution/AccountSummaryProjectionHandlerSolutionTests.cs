﻿using System;
using System.Threading.Tasks;

using Formation.Account.Events;
using Formation.Account.Projection;
using Formation.MongoDb;
using Formation.Tests.Unit.Fixture;
using Formation.Tests.Unit.Helper;

using Xunit;

namespace Formation.Tests.Unit.Account
{
    [Collection(XUnitCollections.MongoDb)]
    public class AccountSummaryProjectionHandlerTestsSolution
    {
        private static readonly Guid DefaultAccountId = Guid.NewGuid();
        private static readonly string DefaultName = "cosmo";

        private readonly MongoRepository<AccountSummaryDocument> _mongoDbRepository;
        private readonly AccountSummaryProjectionHandler _handler;

        public AccountSummaryProjectionHandlerTestsSolution(MongoDbFixture mongoDbFixture)
        {
            this._mongoDbRepository = mongoDbFixture.CreateMongoRepository<AccountSummaryDocument>(AccountSummaryDocument.CreateIndexesAsync);
            this._handler = new AccountSummaryProjectionHandler(this._mongoDbRepository);
        }

        [Fact]
        public async Task When_Account_Created_Then_Create_Projection()
        {
            // When
            var accountCreatedEvent = new AccountCreatedEvent(DefaultAccountId, DefaultName);
            await this._handler.HandleAsync(accountCreatedEvent.ToSubscribed());

            // Assert
            var documents = await this._mongoDbRepository.FindAllAsync();

            var document = Assert.Single(documents);

            Assert.Equal(DefaultAccountId, document.AccountId);
            Assert.Equal(DefaultName, document.AccountName);
            Assert.Equal(0, document.Total);
        }

        // TODO: Update projection on events of income and expense
    }
}
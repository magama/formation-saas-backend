﻿using System;
using System.Linq;

using Formation.Account;
using Formation.Account.Events;
using Formation.Tests.Unit.Fixture;

using Xunit;

namespace Formation.Tests.Unit.Account
{
    public class AccountAggregateTestsSolution : IClassFixture<AppSettingsFixture>
    {
        public AccountAggregateTestsSolution()
        {
        }

        [Fact]
        public void Given_New_Account_When_Create_Account_Then_Account_Created()
        {
            var accountId = Guid.NewGuid();
            var accountName = "potato";

            // Given
            var aggregateState = new AccountAggregateStateSolution();

            var aggregate = new AccountAggregateSolution(aggregateState);

            // When
            var operationResult = aggregate.CreateAccount(accountId, accountName);

            // Then
            Assert.True(operationResult.IsSuccess);

            var events = aggregate.UncommitedEvents.Select(x => x.DomainEvent);

            var singleEvent = Assert.Single(events);
            var createEvent = Assert.IsType<AccountCreatedEvent>(singleEvent);
            Assert.Equal(accountId, createEvent.AccountId);
            Assert.Equal(accountName, createEvent.Name);
        }

        [Fact]
        public void Given_Existing_Account_When_Create_Account_Then_Error()
        {
            var accountId = Guid.NewGuid();
            var accountName = "potato";

            // Given
            var aggregateState = new AccountAggregateStateSolution();

            aggregateState.Apply(new AccountCreatedEvent(accountId, accountName));

            var aggregate = new AccountAggregateSolution(aggregateState);

            // When
            var operationResult = aggregate.CreateAccount(accountId, accountName);

            // Then
            Assert.False(operationResult.IsSuccess);
            Assert.Equal("AlreadyExist" ,operationResult.ErrorCode);
        }

    }
}
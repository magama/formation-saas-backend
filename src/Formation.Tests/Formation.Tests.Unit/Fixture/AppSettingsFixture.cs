﻿using System;
using System.Linq;
using System.Text;

using Formation.Common.Configuration;

using Microsoft.Extensions.Configuration;

namespace Formation.Tests.Unit.Fixture
{
    public sealed class AppSettingsFixture
    {
        public AppSettingsFixture()
        {
            var configuration = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();
            ConfigurationService.SetConfiguration(configuration);
        }
    }
}
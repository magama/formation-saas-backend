﻿using System;
using System.Linq;
using System.Net.Http;
using System.Text;

using Formation.WebApp;

using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;

using Xunit;

namespace Formation.Tests.Unit.Fixture
{
    public sealed class WebHostFixture : IDisposable
    {
        private readonly TestServer _server;

        public WebHostFixture()
        {
            this._server = new TestServer(
                WebHost.CreateDefaultBuilder()
                    .UseStartup<Startup>());

            this.Client = this._server.CreateClient();
            this.Client.BaseAddress = new Uri("https://locahost:5001/");
        }

        public HttpClient Client { get; }

        public void Dispose()
        {
            this._server.Dispose();
            this.Client.Dispose();
        }
    }
}
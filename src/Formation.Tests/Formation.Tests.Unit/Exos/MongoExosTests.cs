﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Formation.MongoDb;
using Formation.Tests.Unit.Fixture;

using MongoDB.Driver;

using Xunit;

namespace Formation.Tests.Unit.Exos
{
    [Collection(XUnitCollections.MongoDb)]
    public class MongoExosTests
    {
        private readonly MongoRepository<TodoNameThisDocument> _repository;

        public MongoExosTests(MongoDbFixture mongoDbFixture)
        {
            this._repository = mongoDbFixture.CreateMongoRepository<TodoNameThisDocument>();
        }

        [Fact]
        public void Do_A_Test_Here()
        {
        }

        // 1- TODO: Fit this class to the collection 
        private class TodoNameThisDocument : MongoDocument
        {
        }
    }
}
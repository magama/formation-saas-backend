﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Formation.MongoDb
{
    public class MongoDuplicateDocumentException : Exception
    {
        public MongoDuplicateDocumentException(string operationName, string documentType, IEnumerable<MongoDocument> foundDuplicates)
            : base($"Error while {operationName}: duplicates for document of type {documentType} where found. The following ids matches the filter {string.Join(", ", foundDuplicates.Select(x => x.Id.ToString()))}")
        {
        }
    }
}
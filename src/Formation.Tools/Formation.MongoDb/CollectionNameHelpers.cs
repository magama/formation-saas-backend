﻿using System;

namespace Formation.MongoDb
{
    public static class CollectionNameHelpers
    {
        public static string GetCollectionNameForDocumentType<TDocument>()
        {
            var documentName = typeof(TDocument).Name;
            var documentSuffixLastIndex = documentName.LastIndexOf("Document", StringComparison.OrdinalIgnoreCase);

            return documentSuffixLastIndex > 0 ? documentName.Substring(0, documentSuffixLastIndex) : documentName;
        }
    }
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Formation.Common.EventSourcing;
using Formation.EventStore.Plumbing;

namespace Formation.EventStore
{
    public interface IEventRepository
    {
        Task<IReadOnlyList<CommitedEvent>> GetStreamEventsAsync(string streamId, long fromVersionInclusive, CancellationToken cancellationToken = default(CancellationToken));

        Task AppendAsync(string streamId, IEnumerable<DomainEvent> events);

        Task SaveAsync(string streamId, IEnumerable<UncommitedEvent> events);

        Task AppendSingleAsync<TDomainEvent>(string streamId, TDomainEvent @event)
            where TDomainEvent : DomainEvent;

        Task<(long NextEventNumber, bool IsEndOfStream, IReadOnlyList<CommitedEvent> Events)> ReadStreamEventsForwardAsync(
            string streamId,
            long fromVersionInclusive,
            int count = 128,
            bool resolveLinkTo = false,
            CancellationToken cancellationToken = default(CancellationToken));
    }
}
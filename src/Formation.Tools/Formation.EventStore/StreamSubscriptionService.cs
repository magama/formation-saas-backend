﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Formation.EventStore.Plumbing;
using Formation.MongoDb;

using SGWA.EventSourcing.EventStore;

namespace Formation.EventStore
{
    public static class StreamSubscriptionService
    {
        private static IPersistStreamCheckpoint StreamCheckpoint;
        private static SubscriptionMonitor Monitor;

        private static bool IsInitialized;

        public static void Initialize(MongoRepository<CheckpointDocument> checkpointRepository, SubscriptionMonitor subscriptionMonitor)
        {
            Monitor = subscriptionMonitor;
            StreamCheckpoint = new MongoDbPersistStreamCheckpoint(checkpointRepository);
            IsInitialized = true;
        }

        public static Task StartStreamCatchUpSubscriptionWithCheckpointPersistance<T>(
            T handler)
        {
            if (!IsInitialized)
            {
                throw  new InvalidOperationException("The service should be initialized before using.");
            }

            return StartSingleStreamCatchUpSubscriptionWithCheckpointPersistance(
                "app-all",
                handler,
                typeof(T).Name);
        }

        private static Task StartSingleStreamCatchUpSubscriptionWithCheckpointPersistance<T>(
            string streamId,
            T handler,
            string subscriptionName)
        {
            var builder = new CatchUpProjectionHandlerBuilder(subscriptionName, Monitor)
                .WithHandlers(handler)
                .WithCheckpointPersistance(StreamCheckpoint)
                .ForStream(streamId);

            return builder.SubscribeFromLastCheckpointAsync(EventStoreConfiguration.GetConnection);
        }
    }
}
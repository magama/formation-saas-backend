﻿using System;
using System.Linq;
using System.Text;

namespace Formation.EventStore
{
    public interface IHandleLiveProcessingStarted
    {
        void HandleLiveProcessingStarted();
    }
}
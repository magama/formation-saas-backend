﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Reflection;
using System.Text;

using Formation.Common.EventSourcing;
using Formation.EventStore.BlackMagic;
using Formation.EventStore.Plumbing;

namespace Formation.EventStore.Aggregate
{
    public abstract class AggregateRoot
    {
        public string StreamId { get; set; }

        /// <summary>
        /// Position of the last written event in the stream.
        /// The -1 value means that no events has been persisted to the stream.
        /// </summary>
        public long StreamPosition { get; protected set; } = -1;

        /// <summary>
        /// Returns the list of events that occured within the aggregate the list of events to be commited.
        /// </summary>
        public IReadOnlyList<UncommitedEvent> UncommitedEvents => new ReadOnlyCollection<UncommitedEvent>(this.UncommitedEventsInternal);

        protected List<UncommitedEvent> UncommitedEventsInternal { get; } = new List<UncommitedEvent>();

        public void ClearUncommitedEvents()
        {
            this.UncommitedEventsInternal.Clear();
        }
    }

    public abstract class AggregateRoot<TState> : AggregateRoot
    {
        private static readonly ConcurrentDictionary<EventInfo, CompiledInstanceMethodInfo> ApplyMethods = new ConcurrentDictionary<EventInfo, CompiledInstanceMethodInfo>();

        [SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors", Justification = "How to reload a given snapshot is only known by the implementation.")]
        protected AggregateRoot(TState stateSolution)
        {
            this.StateSolution = stateSolution;
        }

        public TState StateSolution { get; }

        public void Load(string streamId, IEnumerable<CommitedEvent> history)
        {
            this.StreamId = streamId;
            foreach (var historicalEvent in history)
            {
                this.InvokeApplyMethod(historicalEvent.DomainEvent);
                this.StreamPosition = historicalEvent.StreamPosition;
            }
        }

        /// <summary>
        /// Raises an event to be stored and then published once the operation on the aggregate has been completed.
        /// </summary>
        [SuppressMessage("Microsoft.Design", "CA1030:UseEventsWhereAppropriate", Justification = "This is not a .NET event.")]
        protected void ApplyEvent(DomainEvent @event)
        {
            var position = ++this.StreamPosition;
            this.InvokeApplyMethod(@event);
            this.UncommitedEventsInternal.Add(new UncommitedEvent(this.StreamId, position, @event));
        }

        private static CompiledInstanceMethodInfo GetApplyMethodForEvent(EventInfo info)
        {
            var method = info.StateType.GetMethod("Apply", BindingFlags.Instance | BindingFlags.Public, null, new[] { info.EventType }, null);
            return new CompiledInstanceMethodInfo(method);
        }

        private void InvokeApplyMethod(DomainEvent @event)
        {
            var eventInfo = new EventInfo(this.StateSolution.GetType(), @event.GetType());
            var applyMethod = ApplyMethods.GetOrAdd(eventInfo, GetApplyMethodForEvent);
            applyMethod.Invoke(this.StateSolution, @event);
        }

        private sealed class EventInfo
        {
            public EventInfo(Type stateType, Type eventType)
            {
                this.StateType = stateType;
                this.EventType = eventType;
            }

            public Type StateType { get; }

            public Type EventType { get; }

            /// <summary>Determines whether the specified object is equal to the current object.</summary>
            /// <param name="obj">The object to compare with the current object. </param>
            /// <returns>true if the specified object  is equal to the current object; otherwise, false.</returns>
            public override bool Equals(object obj)
            {
                if (obj is null)
                {
                    return false;
                }

                if (ReferenceEquals(this, obj))
                {
                    return true;
                }

                return obj is EventInfo info && this.Equals(info);
            }

            /// <summary>Serves as the default hash function. </summary>
            /// <returns>A hash code for the current object.</returns>
            public override int GetHashCode()
            {
                unchecked
                {
                    return (this.StateType.GetHashCode() * 397) ^ this.EventType.GetHashCode();
                }
            }

            private bool Equals(EventInfo other)
            {
                return this.StateType == other.StateType && this.EventType == other.EventType;
            }
        }
    }
}
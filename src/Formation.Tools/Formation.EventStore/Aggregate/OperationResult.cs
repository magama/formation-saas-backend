﻿using System;
using System.Linq;
using System.Text;

namespace Formation.EventStore.Aggregate
{
    public class OperationResult
    {
        private OperationResult(bool isSuccess, string errorCode)
        {
            this.IsSuccess = isSuccess;
            this.ErrorCode = errorCode;
        }

        public static OperationResult Success { get; } = new OperationResult(true, null);

        public bool IsSuccess { get; }

        public string ErrorCode { get; }

        public static OperationResult Failure(string errorCode) => new OperationResult(false, errorCode);
    }
}
﻿using System;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Formation.EventStore.Plumbing;

using Serilog;
using Serilog.Context;

namespace Formation.EventStore.Aggregate
{
    public sealed class AggregateInvoker<TAggregate, TState> : IAggregateInvoker<TAggregate, TState>
        where TAggregate : AggregateRoot<TState>, new()
    {
        private readonly IEventRepository _eventRepository;
        private readonly int _maxRetryCount;

        public AggregateInvoker(IEventRepository eventRepository)
            : this(eventRepository, 5)
        {
        }

        public AggregateInvoker(IEventRepository eventRepository, int maxRetryCount)
        {
            this._eventRepository = eventRepository;
            this._maxRetryCount = maxRetryCount;
        }

        public Task<OperationResult> InvokeAsync(string streamId, Func<TAggregate, OperationResult> operation, CancellationToken cancellationToken = new CancellationToken())
        {
            using (LogContext.PushProperty("StreamId", streamId))
            {
                return this.InvokeWithRetryAsync(
                    streamId,
                    ar => Task.FromResult(operation(ar)),
                    cancellationToken);
            }
        }

        private async Task<OperationResult> InvokeWithRetryAsync(
            string streamId,
            Func<TAggregate, Task<OperationResult>> operation,
            CancellationToken cancellationToken)
        {
            var retryCount = 0;

            void OnRetrying(int count) => Log.Warning("ConcurrencyException: {RetryCount} tries.", count);
            void OnRetryExceeded(Exception ex) => Log.Error(ex, "ConcurrencyException: Maximum retry count {MaxRetryCount} limit acheived.", this._maxRetryCount);

            while (true)
            {
                var history = await this._eventRepository.GetStreamEventsAsync(streamId, 0, cancellationToken);
                var aggregate = new TAggregate();
                aggregate.Load(streamId, history);

                var result = await operation(aggregate);
                var events = aggregate.UncommitedEvents;

                try
                {
                    await this._eventRepository.SaveAsync(streamId, events);
                    using (LogContext.PushProperty("EventCount", events.Count))
                    using (LogContext.PushProperty("Events", events, destructureObjects: true))
                    {
                        Log.Debug("EventsSaved");
                    }
                }
                catch (ConcurrencyException ex)
                {
                    if (retryCount < this._maxRetryCount)
                    {
                        ++retryCount;
                        OnRetrying(retryCount);
                        continue;
                    }

                    OnRetryExceeded(ex);
                    throw;
                }

                aggregate.ClearUncommitedEvents();

                return result;
            }
        }
    }
}
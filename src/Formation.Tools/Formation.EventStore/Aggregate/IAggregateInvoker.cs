﻿using System;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Formation.EventStore.Aggregate
{
    public interface IAggregateInvoker<out TAggregate, TState>
        where TAggregate : AggregateRoot<TState>
    {
        Task<OperationResult> InvokeAsync(
            string streamId,
            Func<TAggregate, OperationResult> operation,
            CancellationToken cancellationToken);
    }
}
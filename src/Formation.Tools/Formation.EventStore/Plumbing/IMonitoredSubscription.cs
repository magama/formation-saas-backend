﻿using System;
using System.Linq;
using System.Text;

namespace Formation.EventStore
{
    public interface IMonitoredSubscription
    {
        string SubscriptionName { get; }

        ProcessingStatus ProcessingState { get; }

        SubscriptionStatus SubscriptionState { get; }

        long? CurrentCheckpoint { get; }

        string LastEventStreamId { get; }

        string LastEventType { get; }

        long? LastEventOriginalNumber { get; }

        Exception FailureException { get; }

        void RestartSubscription();
    }
}
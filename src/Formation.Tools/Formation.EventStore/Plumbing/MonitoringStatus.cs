﻿using System;
using System.Linq;
using System.Text;

namespace Formation.EventStore
{

    public enum ProcessingStatus
    {
        Catchup = 1,
        Live = 2,
    }

    public enum SubscriptionStatus
    {
        NotStarted = 0,
        Running = 1,
        Failed = 2,
        Dropped = 3,
    }

}
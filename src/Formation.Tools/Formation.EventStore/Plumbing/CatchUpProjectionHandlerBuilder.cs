﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using EventStore.ClientAPI;

using Formation.Common.EventSourcing;
using Formation.EventStore.BlackMagic;
using Formation.EventStore.Serialisation;

namespace Formation.EventStore.Plumbing
{
    public sealed class CatchUpProjectionHandlerBuilder
    {
        private readonly SubscriptionMonitor _subscriptionMonitor;
        private readonly string _subscriptionName;

        private readonly Dictionary<Type, HandleEvent> _handlers = new Dictionary<Type, HandleEvent>();

        private readonly JsonEventSerializer _eventSerializer;
        private string _streamName;
        private IPersistStreamCheckpoint _persistStreamCheckpoint = new NullPersistStreamCheckpoint();
        private Action _liveProcessingStartedHandler;

        public CatchUpProjectionHandlerBuilder(string subscriptionName, SubscriptionMonitor subscriptionMonitor)
        {
            this._subscriptionName = subscriptionName;
            this._eventSerializer = new JsonEventSerializer();
            this._subscriptionMonitor = subscriptionMonitor;
        }

        public CatchUpProjectionHandlerBuilder ForStream(string streamName)
        {
            this._streamName = streamName;

            return this;
        }

        public CatchUpProjectionHandlerBuilder WithCheckpointPersistance(IPersistStreamCheckpoint persistStreamCheckpoint)
        {
            this._persistStreamCheckpoint = persistStreamCheckpoint;

            return this;
        }

        public CatchUpProjectionHandlerBuilder WithHandlers(object handler)
        {
            var handlerType = handler.GetType();

            var interfaces = handlerType.GetInterfaces();

            if (handler is IHandleLiveProcessingStarted handleLiveProcessingStarted)
            {
                this._liveProcessingStartedHandler = handleLiveProcessingStarted.HandleLiveProcessingStarted;
            }

            foreach (var @interface in interfaces)
            {
                if (@interface.IsConstructedGenericType)
                {
                    var genericTypeDef = @interface.GetGenericTypeDefinition();

                    if (genericTypeDef == typeof(IEventHandler<>))
                    {
                        var eventType = @interface.GenericTypeArguments[0];

                        var loggedHandlerType = typeof(LoggedEventHandler);
                        var loggedHandlerGenericMethod = loggedHandlerType.GetMethod(nameof(LoggedEventHandler.HandleAsync), BindingFlags.Static | BindingFlags.Public);
                        var loggedHandlerMethod = loggedHandlerGenericMethod.MakeGenericMethod(eventType);

                        var handlerMethod = new CompiledStaticMethodInfo(loggedHandlerMethod);

                        var subscribedEventCtor = typeof(SubscribedEvent<>).MakeGenericType(eventType)
                            .GetConstructor(
                                new[]
                                {
                                    typeof(Guid),
                                    eventType,
                                    typeof(string),
                                    typeof(long),
                                });

                        var compiledSubscribedEventCtor = SubscribedEventsConstructorCache.Instance.GetOrAdd(eventType, _ => new CompiledCtorInfo(subscribedEventCtor));

                        Task HandleTypedEvent(
                            Guid eventId,
                            DomainEvent @event,
                            string streamId,
                            long eventNumber,
                            CancellationToken t)
                        {

                            var subcribed = compiledSubscribedEventCtor.Invoke(
                                eventId,
                                @event,
                                streamId,
                                eventNumber
                                );

                            return (Task)handlerMethod.Invoke(
                                handler,
                                subcribed,
                                default(CancellationToken));
                        }

                        this._handlers.Add(eventType, HandleTypedEvent);
                    }
                }
            }

            return this;
        }

        public async Task<CatchUpProjectionHandler> SubscribeFromLastCheckpointAsync(Func<IEventStoreConnection> connection)
        {
            var checkpoint = await this._persistStreamCheckpoint.GetCheckpointAsync(this._subscriptionName);

            var projection = this.CreateProjectionHandler(connection);
            projection.SubscribeFrom(checkpoint);

            return projection;
        }

        public Task<CatchUpProjectionHandler> SubscribeAsync(Func<IEventStoreConnection> connection)
        {
            var projection = this.CreateProjectionHandler(connection);
            projection.SubscribeFrom(null);

            return Task.FromResult(projection);
        }

        private CatchUpProjectionHandler CreateProjectionHandler(Func<IEventStoreConnection> connection)
        {
            var projectionHandler = new CatchUpProjectionHandler(
                this._subscriptionName,
                this._streamName,
                connection,
                this._persistStreamCheckpoint,
                this._handlers,
                this._liveProcessingStartedHandler,
                this._eventSerializer);
            this._subscriptionMonitor.Append(projectionHandler);

            return projectionHandler;
        }
    }
}
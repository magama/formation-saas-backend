﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Formation.EventStore
{
    public class NullPersistStreamCheckpoint : IPersistStreamCheckpoint
    {
        public Task<long?> GetCheckpointAsync(string subscription)
        {
            return Task.FromResult((long?)0);
        }

        public Task SaveCheckpointAsync(string subscription, long? eventNumber, long? originalEventNumber)
        {
            return Task.CompletedTask;
        }
    }
}
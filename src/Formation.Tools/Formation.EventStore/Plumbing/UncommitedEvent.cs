﻿using System;
using System.Linq;
using System.Text;

using Formation.Common.EventSourcing;

namespace Formation.EventStore.Plumbing
{
    public sealed class UncommitedEvent
    {
        public UncommitedEvent(string streamId, long streamPosition, DomainEvent domainEvent)
        {
            this.EventId = Guid.NewGuid();
            this.StreamId = streamId;
            this.StreamPosition = streamPosition;
            this.DomainEvent = domainEvent;
        }

        public Type EventType => this.DomainEvent.GetType();

        public Guid EventId { get; }

        public string StreamId { get; }

        public long StreamPosition { get; }

        public DomainEvent DomainEvent { get; }
    }
}
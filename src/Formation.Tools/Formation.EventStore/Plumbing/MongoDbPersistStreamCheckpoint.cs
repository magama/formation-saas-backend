﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Formation.MongoDb;

using MongoDB.Driver;

using SGWA.EventSourcing.EventStore;

namespace Formation.EventStore.Plumbing
{
    public sealed class MongoDbPersistStreamCheckpoint : IPersistStreamCheckpoint
    {
        public const string CollectionName = "ProjectionHandlerCheckpoints";

        private readonly MongoRepository<CheckpointDocument> _checkpointRepository;

        public MongoDbPersistStreamCheckpoint(MongoRepository<CheckpointDocument> mongoRepository)
        {
            this._checkpointRepository = mongoRepository;
        }

        public async Task<long?> GetCheckpointAsync(string subscription)
        {
            var subscriptionFilter = Builders<CheckpointDocument>.Filter.Eq(x => x.SubscriptionName, subscription);
            var checkpoint = await this._checkpointRepository.FindOneAsync(subscriptionFilter);
            return checkpoint?.EventNumber;
        }

        public Task SaveCheckpointAsync(string subscription, long? eventNumber, long? originalEventNumber)
        {
            var subscriptionFilter = Builders<CheckpointDocument>.Filter.Eq(x => x.SubscriptionName, subscription);
            var updateDefinition = GetCheckpointUpsertDefinition(subscription, eventNumber, originalEventNumber);
            return this._checkpointRepository.UpsertOneAsync(subscriptionFilter, updateDefinition);
        }

        private static UpdateDefinition<CheckpointDocument> GetCheckpointUpsertDefinition(string subscription, long? eventNumber, long? originalEventNumber)
        {
            var subscriptionNameUpdate = Builders<CheckpointDocument>.Update.SetOnInsert(x => x.SubscriptionName, subscription);
            var eventNumberUpdate = Builders<CheckpointDocument>.Update.Set(x => x.EventNumber, eventNumber);
            var originaleventNumberUpdate = Builders<CheckpointDocument>.Update.Set(x => x.OriginalEventNumber, originalEventNumber);
            return Builders<CheckpointDocument>.Update.Combine(subscriptionNameUpdate, eventNumberUpdate, originaleventNumberUpdate);
        }
    }
}
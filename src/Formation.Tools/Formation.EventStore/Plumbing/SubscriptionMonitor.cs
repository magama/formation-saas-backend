﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Formation.EventStore
{
    public class SubscriptionMonitor
    {
        private readonly List<IMonitoredSubscription> _monitoredSubscriptions = new List<IMonitoredSubscription>();

        public IReadOnlyList<IMonitoredSubscription> MonitoredSubscriptions => this._monitoredSubscriptions.AsReadOnly();

        public void Append(IMonitoredSubscription monitoredSubscription)
        {
            this._monitoredSubscriptions.Add(monitoredSubscription);
        }
    }
}
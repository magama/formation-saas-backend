﻿using System;
using System.Linq;
using System.Text;

using Formation.Common.EventSourcing;

namespace Formation.EventStore.Plumbing
{
    public class CommitedEvent
    {
        public CommitedEvent(Guid eventId, string streamId, long streamPosition, DomainEvent domainEvent)
        {
            this.EventId = eventId;
            this.StreamId = streamId;
            this.StreamPosition = streamPosition;
            this.DomainEvent = domainEvent;
        }

        public Guid EventId { get; }

        public string StreamId { get; }

        public long StreamPosition { get; }

        public DomainEvent DomainEvent { get; }
    }
}
﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Formation.EventStore
{
    public interface IPersistStreamCheckpoint
    {
        Task<long?> GetCheckpointAsync(string subscription);

        Task SaveCheckpointAsync(string subscription, long? eventNumber, long? originalEventNumber);
    }
}
﻿using System;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Formation.Common.EventSourcing;

using Serilog;
using Serilog.Context;

namespace Formation.EventStore
{
    public static class LoggedEventHandler
    {
        public static Task HandleAsync<TEvent>(IEventHandler<TEvent> next, SubscribedEvent<TEvent> subscribed, CancellationToken cancellationToken = default(CancellationToken))
            where TEvent : DomainEvent
        {
            using (LogContext.PushProperty("EventHandler", next.GetType().Name))
            using (LogContext.PushProperty("EventId", subscribed.EventId))
            using (LogContext.PushProperty("DomainEvent", subscribed.Event, destructureObjects: true))
            {
                try
                {
                    return next.HandleAsync(subscribed, cancellationToken);
                }
                catch (Exception exception)
                {
                    Log.Error(exception, "An error occured while handling event");
                    throw;
                }
            }
        }
    }
}
﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Formation.MongoDb;

using MongoDB.Driver;

namespace SGWA.EventSourcing.EventStore
{
    public class CheckpointDocument : MongoDocument
    {
        public string SubscriptionName { get; set; }

        public long? EventNumber { get; set; }

        public long? OriginalEventNumber { get; set; }

        public static async Task CreateIndexesAsync(IMongoCollection<CheckpointDocument> collection)
        {
            var index = Builders<CheckpointDocument>.IndexKeys.Ascending(x => x.SubscriptionName);

            // Make index case insensitive https://docs.mongodb.com/manual/reference/collation/
            var collation = new Collation("en", strength: CollationStrength.Primary);
            await collection.Indexes.CreateOneAsync(index, new CreateIndexOptions { Unique = true, Collation = collation });
        }
    }
}
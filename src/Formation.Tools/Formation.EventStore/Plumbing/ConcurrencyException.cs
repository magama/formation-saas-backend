﻿using System;
using System.Linq;
using System.Text;

namespace Formation.EventStore.Plumbing
{
    [Serializable]
    public class ConcurrencyException : Exception
    {
        public ConcurrencyException(string streamId, long expectedVersion, Exception inner)
            : base($"The expected version ({expectedVersion}) of stream '{streamId}' was different on the server. {inner.Message}", inner)
        {
        }
    }
}
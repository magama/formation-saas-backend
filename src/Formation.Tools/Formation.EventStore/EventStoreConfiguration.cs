﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using EventStore.ClientAPI;

using Formation.Common.Configuration;

using Serilog;

using ILogger = EventStore.ClientAPI.ILogger;

namespace Formation.EventStore
{
    public static class EventStoreConfiguration
    {
        private static readonly object ConnectionInstanceAccessLock = new object();
        private static IEventStoreConnection fastFailingConnectionInstance;

        private static string ConnectionString => "ConnectTo=tcp://{username}:{password}@{host}:{port}"
            .Replace("{username}", ConfigurationService.GetSetting("EventStore:Username"))
            .Replace("{password}", ConfigurationService.GetSetting("EventStore:Password"))
            .Replace("{host}", ConfigurationService.GetSetting("EventStore:Host"))
            .Replace("{port}", ConfigurationService.GetSetting("EventStore:TcpPort"));

        /// <summary>
        /// Operation with this connection while the event store is disconnected will throw an exception
        /// </summary>
        public static IEventStoreConnection GetConnection()
        {
            lock (ConnectionInstanceAccessLock)
            {
                if (fastFailingConnectionInstance == null)
                {
                    fastFailingConnectionInstance = CreateFastFailingConnection();
                    fastFailingConnectionInstance.Closed += OnFastFailingConnectionClosed;
                    Task.Run(() => fastFailingConnectionInstance.ConnectAsync()).Wait();
                }

                return fastFailingConnectionInstance;
            }
        }

        private static ConnectionSettingsBuilder GetConnectionSettingsBuilder(string connectionName)
        {
            var useSSL = ConfigurationService.GetSetting<bool>("EventStore:UseSSL");
            var connectionSettingsBuilder = ConnectionSettings.Create()
                .UseCustomLogger(new SerilogLoggerAdapter(connectionName))
                .KeepReconnecting();

            if (useSSL)
            {
                connectionSettingsBuilder = connectionSettingsBuilder.UseSslConnection("share-gate.com", false);
            }

            return connectionSettingsBuilder;
        }

        private static IEventStoreConnection CreateFastFailingConnection()
        {
            return EventStoreConnection.Create(
                ConnectionString,
                GetConnectionSettingsBuilder("FastFailing").LimitReconnectionsTo(1),
                "FastFailing");
        }

        private static void OnFastFailingConnectionClosed(object sender, ClientClosedEventArgs clientClosedEventArgs)
        {
            lock (ConnectionInstanceAccessLock)
            {
                fastFailingConnectionInstance.Closed -= OnFastFailingConnectionClosed;
                fastFailingConnectionInstance = null;
            }
        }

        private sealed class SerilogLoggerAdapter : ILogger
        {
            private readonly Serilog.ILogger _logger;

            public SerilogLoggerAdapter(string connectionName)
            {
                this._logger = Log.ForContext("EventStore", connectionName);
            }

            public void Error(string format, params object[] args) => this._logger.Error(format, args);

            public void Error(Exception ex, string format, params object[] args) => this._logger.Error(ex, format, args);

            public void Info(string format, params object[] args) => this._logger.Information(format, args);

            public void Info(Exception ex, string format, params object[] args) => this._logger.Information(ex, format, args);

            public void Debug(string format, params object[] args) => this._logger.Debug(format, args);

            public void Debug(Exception ex, string format, params object[] args) => this._logger.Debug(ex, format, args);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using EventStore.ClientAPI;
using EventStore.ClientAPI.Exceptions;

using Formation.Common.EventSourcing;
using Formation.EventStore.Plumbing;
using Formation.EventStore.Serialisation;

namespace Formation.EventStore
{
    public class EventStoreRepository : IEventRepository
    {
        private readonly IEventSerializer _eventSerializer;

        private readonly Func<IEventStoreConnection> _getEventStoreConnection;

        public EventStoreRepository(Func<IEventStoreConnection> getEventStoreConnection)
            : this(getEventStoreConnection, new JsonEventSerializer())
        {
        }
 
        public EventStoreRepository(Func<IEventStoreConnection> getEventStoreConnection, IEventSerializer eventSerializer)
        {
            this._getEventStoreConnection = getEventStoreConnection;
            this._eventSerializer = eventSerializer;
        }

        private IEventStoreConnection EventStoreConnection => this._getEventStoreConnection();

        public async Task AppendAsync(string streamId, IEnumerable<DomainEvent> events)
        {
            var eventData = events.Select(e => this.AppendEventToEventDataConverter(e)).ToList();

            if (eventData.Any())
            {
                var expectedVersion = ExpectedVersion.Any;

                try
                {
                    await this.EventStoreConnection.AppendToStreamAsync(streamId, expectedVersion, eventData);
                }
                catch (WrongExpectedVersionException ex)
                {
                    throw new ConcurrencyException(streamId, expectedVersion, ex);
                }
                catch (Exception exception) when (exception is ObjectDisposedException || exception is ConnectionClosedException)
                {
                    // An ObjectDisposedException occured if the connection is already closed
                    throw new ConnectionClosedException("Connection to the event store is temporaly disconnected.", exception);
                }
            }
        }

        public Task AppendSingleAsync<TDomainEvent>(string streamId, TDomainEvent @event)
            where TDomainEvent : DomainEvent
        {
            return this.AppendAsync(streamId, new DomainEvent[] { @event });
        }

        public async Task SaveAsync(string streamId, IEnumerable<UncommitedEvent> events)
        {
            var uncommitedEvents = events as IList<UncommitedEvent> ?? events.ToList();

            if (uncommitedEvents.Any())
            {

                var eventData = uncommitedEvents.Select(this.UncommitedEventToEventDataConverter).ToList();
                var expectedVersion = uncommitedEvents.First().StreamPosition - 1;

                try
                {
                    await this.EventStoreConnection.AppendToStreamAsync(streamId, expectedVersion, eventData);
                }
                catch (WrongExpectedVersionException ex)
                {
                    throw new ConcurrencyException(streamId, expectedVersion, ex);
                }
                catch (Exception exception) when (exception is ObjectDisposedException || exception is ConnectionClosedException)
                {
                    // An ObjectDisposedException occured if the connection is already closed
                    throw new ConnectionClosedException("Connection to the event store is temporaly disconnected.", exception);
                }
            }
        }

        public async Task<IReadOnlyList<CommitedEvent>> GetStreamEventsAsync(
            string streamId,
            long fromVersionInclusive,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            var events = new List<CommitedEvent>();
            bool isEndOfStream;

            do
            {
                var (nextEventNumber, isEOS, xs) = await this.ReadStreamEventsForwardAsync(streamId, fromVersionInclusive, cancellationToken: cancellationToken);

                fromVersionInclusive = nextEventNumber;
                isEndOfStream = isEOS;
                events.AddRange(xs);
            }
            while (!isEndOfStream);

            cancellationToken.ThrowIfCancellationRequested();
            return events.AsReadOnly();
        }

        public async Task<(long NextEventNumber, bool IsEndOfStream, IReadOnlyList<CommitedEvent> Events)> ReadStreamEventsForwardAsync(string streamId, long fromVersionInclusive, int count = 128, bool resolveLinkTo = false, CancellationToken cancellationToken = default(CancellationToken))
        {
            cancellationToken.ThrowIfCancellationRequested();

            try
            {
                var slice = await this.EventStoreConnection.ReadStreamEventsForwardAsync(streamId, fromVersionInclusive, 128, false);
                return (slice.NextEventNumber, slice.IsEndOfStream, slice.Events.Select(this.ResolvedEventToCommitedEventConverter).ToList().AsReadOnly());
            }
            catch (Exception exception) when (exception is ObjectDisposedException || exception is ConnectionClosedException)
            {
                // An ObjectDisposedException occured if the connection is already closed
                throw new ConnectionClosedException("Connection to the event store is temporaly disconnected.", exception);
            }
        }

        private CommitedEvent ResolvedEventToCommitedEventConverter(ResolvedEvent resolvedEvent)
        {
            var recordedEvent = resolvedEvent.Event;

            var eventType = JsonEventTypeMapper.FromTypeName(recordedEvent.EventType);
            var domainEvent = (DomainEvent)this._eventSerializer.FromByteArray(eventType, recordedEvent.Data);

            var metaDataBuffer = recordedEvent.Metadata;

            return new CommitedEvent(
                recordedEvent.EventId,
                recordedEvent.EventStreamId,
                recordedEvent.EventNumber,
                domainEvent);
        }

        private EventData UncommitedEventToEventDataConverter(UncommitedEvent uncommittedEvent)
        {
            var typeString = JsonEventTypeMapper.GetTypeName(uncommittedEvent.EventType);
            var eventBytes = this._eventSerializer.ToByteArray(uncommittedEvent.DomainEvent);

            return new EventData(uncommittedEvent.EventId, typeString, true, eventBytes, Array.Empty<byte>());
        }

        private EventData AppendEventToEventDataConverter(DomainEvent domainEvent)
        {
            var typeString = JsonEventTypeMapper.GetTypeName(domainEvent.GetType());
            var eventBytes = this._eventSerializer.ToByteArray(domainEvent);

            return new EventData(Guid.NewGuid(), typeString, true, eventBytes, Array.Empty<byte>());
        }
    }
}
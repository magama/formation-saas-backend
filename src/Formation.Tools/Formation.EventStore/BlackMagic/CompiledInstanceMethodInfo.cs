﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;

namespace Formation.EventStore.BlackMagic
{
    /// <summary>
    /// Dynamic method compiled for faster execution. Based on
    /// <see href="https://github.com/gautema/CQRSlite/blob/master/Framework/CQRSlite/Infrastructure/CompiledMethodInfo.cs" />.
    /// </summary>
    public sealed class CompiledInstanceMethodInfo
    {
        private readonly Func<object, object[], object> _func;

        public CompiledInstanceMethodInfo(MethodInfo methodInfo)
        {
            var instanceExpression = Expression.Parameter(typeof(object));
            var argumentsExpression = Expression.Parameter(typeof(object[]));
            var parameterInfos = methodInfo.GetParameters();

            var argumentExpressions = new Expression[parameterInfos.Length];
            for (var i = 0; i < parameterInfos.Length; ++i)
            {
                var parameterInfo = parameterInfos[i];
                argumentExpressions[i] = Expression.Convert(Expression.ArrayIndex(argumentsExpression, Expression.Constant(i)), parameterInfo.ParameterType);
            }

            var callExpression = Expression.Call(Expression.Convert(instanceExpression, methodInfo.DeclaringType), methodInfo, argumentExpressions);
            if (callExpression.Type == typeof(void))
            {
                var label = Expression.Label(typeof(object));
                var defaultValue = Expression.Default(typeof(object));
                var actionAsFunc = Expression.Block(callExpression, Expression.Return(label, defaultValue), Expression.Label(label, defaultValue));
                this._func = Expression.Lambda<Func<object, object[], object>>(actionAsFunc, instanceExpression, argumentsExpression).Compile();
            }
            else
            {
                this._func = Expression.Lambda<Func<object, object[], object>>(Expression.Convert(callExpression, typeof(object)), instanceExpression, argumentsExpression)
                    .Compile();
            }
        }

        public object Invoke(object instance, params object[] arguments)
        {
            return this._func(instance, arguments);
        }
    }
}
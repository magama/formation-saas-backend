﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;

namespace Formation.EventStore.BlackMagic
{
    public static class SubscribedEventsConstructorCache
    {
        public static ConcurrentDictionary<Type, CompiledCtorInfo> Instance { get; } = new ConcurrentDictionary<Type, CompiledCtorInfo>();
    }
}
﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;

namespace Formation.EventStore.BlackMagic
{
    /// <summary>
    /// Dynamic method compiled for faster execution. Based on
    /// <see href="https://github.com/gautema/CQRSlite/blob/master/Framework/CQRSlite/Infrastructure/CompiledMethodInfo.cs" />.
    /// </summary>
    public sealed class CompiledStaticMethodInfo
    {
        private readonly Func<object[], object> _func;

        public CompiledStaticMethodInfo(MethodInfo methodInfo)
        {
            var argumentsExpression = Expression.Parameter(typeof(object[]));
            var parameterInfos = methodInfo.GetParameters();

            var argumentExpressions = new Expression[parameterInfos.Length];
            for (var i = 0; i < parameterInfos.Length; ++i)
            {
                var parameterInfo = parameterInfos[i];
                argumentExpressions[i] = Expression.Convert(Expression.ArrayIndex(argumentsExpression, Expression.Constant(i)), parameterInfo.ParameterType);
            }

            var callExpression = Expression.Call(methodInfo, argumentExpressions);
            if (callExpression.Type == typeof(void))
            {
                var label = Expression.Label(typeof(object));
                var defaultValue = Expression.Default(typeof(object));
                var actionAsFunc = Expression.Block(callExpression, Expression.Return(label, defaultValue), Expression.Label(label, defaultValue));
                this._func = Expression.Lambda<Func<object[], object>>(actionAsFunc, argumentsExpression).Compile();
            }
            else
            {
                this._func = Expression.Lambda<Func<object[], object>>(callExpression, argumentsExpression)
                    .Compile();
            }
        }

        public object Invoke(params object[] arguments)
        {
            return this._func(arguments);
        }
    }
}
﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;

namespace Formation.EventStore.BlackMagic
{
    /// <summary>
    /// Dynamic method compiled for faster execution. Based on
    /// <see href="https://github.com/gautema/CQRSlite/blob/master/Framework/CQRSlite/Infrastructure/CompiledMethodInfo.cs" />.
    /// </summary>
    public sealed class CompiledCtorInfo
    {
        private readonly Func<object[], object> _func;

        public CompiledCtorInfo(ConstructorInfo ctorInfo)
        {
            var instanceExpression = Expression.Parameter(typeof(object));
            var argumentsExpression = Expression.Parameter(typeof(object[]));
            var parameterInfos = ctorInfo.GetParameters();

            var argumentExpressions = new Expression[parameterInfos.Length];
            for (var i = 0; i < parameterInfos.Length; ++i)
            {
                var parameterInfo = parameterInfos[i];
                argumentExpressions[i] = Expression.Convert(Expression.ArrayIndex(argumentsExpression, Expression.Constant(i)), parameterInfo.ParameterType);
            }

            var callExpression = Expression.New(ctorInfo, argumentExpressions);

            this._func = Expression.Lambda<Func<object[], object>>(Expression.Convert(callExpression, typeof(object)), argumentsExpression).Compile();
        }

        public object Invoke(params object[] arguments)
        {
            return this._func(arguments);
        }
    }
}
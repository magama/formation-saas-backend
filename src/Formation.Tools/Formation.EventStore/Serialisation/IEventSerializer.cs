﻿using System;
using System.Linq;
using System.Text;

namespace Formation.EventStore.Serialisation
{
    public interface IEventSerializer
    {
        object FromByteArray(Type eventType, byte[] recordedEventData);

        byte[] ToByteArray(object @event);
    }
}
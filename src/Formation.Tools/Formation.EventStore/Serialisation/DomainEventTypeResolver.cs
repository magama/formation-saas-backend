﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Formation.EventStore
{
    public class DomainEventTypeResolver
    {
        private static readonly ConcurrentDictionary<string, Type> StringToTypeCache = new ConcurrentDictionary<string, Type>();
        private readonly Type[] _domainEventTypes;

        public DomainEventTypeResolver(Type domainEventBaseType)
        {
            this._domainEventTypes = GetDomainEventTypes(domainEventBaseType).ToArray();
        }

        public Type GetDomainEventFromTypeName(string typeName)
        {
            return StringToTypeCache.GetOrAdd(typeName, this.InternalGetDomainEventFromTypeName);
        }

        private static IEnumerable<Type> GetDomainEventTypes(Type domainEventType)
        {
            bool TypeImplementsDomainEvent(Type type)
            {
                if (type.BaseType == null)
                {
                    return false;
                }

                if (type.BaseType != domainEventType)
                {
                    return TypeImplementsDomainEvent(type.BaseType);
                }

                return true;
            }

            var assemblies = AppDomain.CurrentDomain.GetAssemblies();
            return assemblies
                .SelectMany(x => x.GetTypes())
                .Where(TypeImplementsDomainEvent);
        }

        private Type InternalGetDomainEventFromTypeName(string typeName)
        {
            var types = this._domainEventTypes.Where(x => x.Name == typeName).ToArray();
            if (types.Length == 0)
            {
                throw new TypeLoadException($"No event type matching name '{typeName}'");
            }

            if (types.Length > 1)
            {
                throw new TypeLoadException($"Multiple event types matching name '{typeName}'");
            }

            return types.Single();
        }
    }
}
﻿using System;
using System.Linq;
using System.Text;

using Formation.Common.EventSourcing;

namespace Formation.EventStore
{
    public static class JsonEventTypeMapper
    {
        private static readonly Lazy<DomainEventTypeResolver> TypeResolver = new Lazy<DomainEventTypeResolver>(() => new DomainEventTypeResolver(typeof(DomainEvent)));

        public static Type FromTypeName(string typeName)
        {
            return TypeResolver.Value.GetDomainEventFromTypeName(typeName);
        }

        public static string GetTypeName(Type type) => type.Name;
    }
}
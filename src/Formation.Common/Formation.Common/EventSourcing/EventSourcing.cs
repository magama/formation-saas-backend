﻿using System;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Formation.Common.EventSourcing
{
    public interface IEventHandler<TEvent>
        where TEvent : DomainEvent
    {
        Task HandleAsync(SubscribedEvent<TEvent> subscribed, CancellationToken cancellationToken = default(CancellationToken));
    }
}
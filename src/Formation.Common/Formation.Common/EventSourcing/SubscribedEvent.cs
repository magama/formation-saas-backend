﻿using System;
using System.Linq;
using System.Text;

namespace Formation.Common.EventSourcing
{
    public class SubscribedEvent<TEvent>
        where TEvent : DomainEvent
    {

        public SubscribedEvent(Guid eventId, TEvent @event, string eventStreamId, long eventNumber)
        {
            this.EventId = eventId;
            this.EventStreamId = eventStreamId;
            this.EventNumber = eventNumber;
            this.Event = @event;
        }

        public long EventNumber { get; }

        public string EventStreamId { get; }

        public Guid EventId { get; }

        public TEvent Event { get; }

        public string EventLocation => $"{this.EventStreamId}/{this.EventNumber}";
    }
}
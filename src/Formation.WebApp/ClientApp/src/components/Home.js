import React from 'react';
import { connect } from 'react-redux';

const Home = props => (
  <div>
    <h1>WWC Bank!</h1>
        <a href='/swagger'>Bring me to Swagger Please</a>
  </div>
);

export default connect()(Home);

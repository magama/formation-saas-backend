﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Formation.Account.Projection;
using Formation.Account.Test;
using Formation.EventStore;
using Formation.EventStore.Plumbing;
using Formation.MongoDb;

using Microsoft.Extensions.DependencyInjection;

using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;

using SGWA.EventSourcing.EventStore;

namespace Formation.WebApp
{
    internal static class SetupService
    {
        internal static async Task ConfigureAsync(IServiceCollection services)
        {
            // Ensure that DateTimeOffSet is serialized like a date
            BsonSerializer.RegisterSerializer(typeof(DateTimeOffset), new DateTimeOffsetSerializer(BsonType.Document));

            // Configure Event Store Connection
            var checkpointRepository = new MongoRepository<CheckpointDocument>(CheckpointDocument.CreateIndexesAsync);
            var subscriptionMonitor = new SubscriptionMonitor();
            StreamSubscriptionService.Initialize(checkpointRepository, subscriptionMonitor);

            var eventStoreRepository = new EventStoreRepository(EventStoreConfiguration.GetConnection);
            services.AddSingleton(eventStoreRepository);

            // Configuration to test setup
            var testRepository = new MongoRepository<TestDocument>(TestDocument.CreateIndexesAsync);
            services.AddSingleton(testRepository);

            var testProjectionHandler = new TestProjectionHandler(testRepository);

            await StreamSubscriptionService.StartStreamCatchUpSubscriptionWithCheckpointPersistance(
                testProjectionHandler);


            // Configuration for bank application
            var accountSummaryRepository = new MongoRepository<AccountSummaryDocument>(AccountSummaryDocument.CreateIndexesAsync);
            services.AddSingleton(accountSummaryRepository);

            var accountSummaryProjectionHandler = new AccountSummaryProjectionHandler(accountSummaryRepository);

            await StreamSubscriptionService.StartStreamCatchUpSubscriptionWithCheckpointPersistance(
                accountSummaryProjectionHandler);

        }
    }
}

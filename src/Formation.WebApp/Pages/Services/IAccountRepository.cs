﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Formation.WebApp.Models;

namespace Formation.WebApp.Pages.Services
{
    public interface IAccountRepository
    {
        Task<Models.Account> CreateAccount(string accountName);

        Task<Models.Account> GetAccount(Guid accountId);

        Task<IEnumerable<Models.Account>> GetAllAccounts();
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Formation.WebApp.Models;

namespace Formation.WebApp.Pages.Services
{
    public class AccountRepository : IAccountRepository
    {
        private readonly IList<Models.Account> _accounts = new List<Models.Account>();

        public Task<Models.Account> CreateAccount(string accountName)
        {
            var account = new Models.Account(accountName);

            this._accounts.Add(account);

            return Task.FromResult(account);
        }

        public Task<Models.Account> GetAccount(Guid accountId)
        {
            var account = this._accounts.FirstOrDefault(x => x.Id == accountId);

            return Task.FromResult(account);
        }

        public Task<IEnumerable<Models.Account>> GetAllAccounts()
        {
            return Task.FromResult(this._accounts.AsEnumerable());
        }
    }
}

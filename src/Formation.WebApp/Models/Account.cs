﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Formation.WebApp.Models
{
    public class Account
    {
        private readonly List<Transaction> _transactions = new List<Transaction>();

        public Account(string name)
        {
            this.Name = name;
        }

        public Guid Id { get; } = Guid.NewGuid();

        public string Name { get; set; }

        public IReadOnlyCollection<Transaction> Transactions => this._transactions.AsReadOnly();

        public void AddTransaction(Transaction transaction)
        {
            this._transactions.Add(transaction);
        }

        public decimal GetBalance()
        {
            var deposits = this._transactions.Where(x => x.Category == TransactionCategory.Deposit).Sum(x => x.Amount);
            var withdrawals = this._transactions.Where(x => x.Category == TransactionCategory.Withdrawal).Sum(x => x.Amount);

            return deposits - withdrawals;
        }
    }
}

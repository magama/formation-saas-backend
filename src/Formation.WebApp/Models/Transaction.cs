﻿using System.ComponentModel.DataAnnotations;

namespace Formation.WebApp.Models
{
    public class Transaction
    {
        [Required]
        public decimal Amount { get; }

        [Required]
        public string Name { get; }

        [Required]
        public TransactionCategory Category { get; }
    }
}

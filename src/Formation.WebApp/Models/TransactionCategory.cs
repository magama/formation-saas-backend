﻿namespace Formation.WebApp.Models
{
    public enum TransactionCategory
    {
        None = 0,
        Withdrawal = 1,
        Deposit = 2,
    }
}

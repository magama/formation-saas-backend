using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

using Formation.Account;
using Formation.Account.Events;
using Formation.Account.Projection;
using Formation.EventStore;
using Formation.EventStore.Aggregate;
using Formation.MongoDb;

using Microsoft.AspNetCore.Mvc;

using MongoDB.Driver;

namespace Formation.WebApp.Controllers
{
    [Route("api/EventSourcingAccounts")]
    public class EventSourcingAccountsController : Controller
    {
        private readonly MongoRepository<AccountSummaryDocument> _accountRepository;
        private readonly EventStoreRepository _eventStoreRepository;
        private readonly AggregateInvoker<AccountAggregate, AccountAggregateState> _accountAggregateInvoker;


        public EventSourcingAccountsController(MongoRepository<AccountSummaryDocument> accountRepository, EventStoreRepository eventStoreRepository)
        {
            this._accountRepository = accountRepository;
            this._eventStoreRepository = eventStoreRepository;

            this._accountAggregateInvoker = new AggregateInvoker<AccountAggregate, AccountAggregateState>(eventStoreRepository);
        }

        [HttpPost("create/{accountName}")]
        public ActionResult CreateAccount([FromRoute, Required] string accountName)
        {
            return this.Ok();
        }

        [HttpGet("{accountId}")]
        public AccountSummaryDocument GetAccount(Guid accountId)
        {
            return default(AccountSummaryDocument);
        }

        [HttpGet]
        public IEnumerable<AccountSummaryDocument> GetAccounts()
        {
            return default(IEnumerable<AccountSummaryDocument>);
        }

        // Little hack so the same account name generate same guid
        private static Guid HashAccountName(string accountName)
        {
            using (MD5 md5 = MD5.Create())
            {
                byte[] hash = md5.ComputeHash(Encoding.Default.GetBytes(accountName));
                return new Guid(hash);
            }
        }
    }
}
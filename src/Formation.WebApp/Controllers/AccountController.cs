﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

using Formation.WebApp.Models;
using Formation.WebApp.Pages.Services;

using Microsoft.AspNetCore.Mvc;

namespace Formation.WebApp.Controllers
{
    [Route("accounts")]
    public class AccountController : ControllerBase
    {
        private readonly IAccountRepository _accountRepository;

        public AccountController(IAccountRepository accountRepository)
        {
            this._accountRepository = accountRepository;
        }

        [HttpPost]
        [Route("{accountName}")]
        public async Task<IActionResult> CreateAccount([FromRoute, Required] string accountName)
        {
            var account = await this._accountRepository.CreateAccount(accountName);

            return this.Ok(account.Id);
        }

        [HttpGet]
        [Route("{accountId}/balance")]
        public async Task<IActionResult> GetAccountBalance([FromRoute, Required] Guid accountId)
        {
            var account = await this._accountRepository.GetAccount(accountId);
            var balance = account.GetBalance();

            return this.Ok(balance);
        }

        [HttpGet]
        public async Task<IActionResult> GetAllAccounts()
        {
            var accounts = await this._accountRepository.GetAllAccounts();

            return this.Ok(accounts);
        }

        [HttpPatch]
        [Route("{accountId}")]
        public async Task<IActionResult> UpdateAccount([FromRoute, Required] Guid accountId, [FromBody, Required] string accountName)
        {
            var account = await this._accountRepository.GetAccount(accountId);

            account.Name = accountName;

            return this.Ok();
        }

        [HttpGet]
        [Route("{accountId}/transactions")]
        public async Task<IActionResult> GetTransactions([FromRoute, Required] Guid accountId)
        {
            var account = await this._accountRepository.GetAccount(accountId);

            return this.Ok(account.Transactions);
        }

        [HttpPost]
        [Route("{accountId}/transactions")]
        public async Task<IActionResult> AddTransaction([FromRoute, Required] Guid accountId, [FromBody, Required] Transaction transaction)
        {
            var account = await this._accountRepository.GetAccount(accountId);

            account.AddTransaction(transaction);

            return this.Ok();
        }
    }
}

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

using Formation.Account;
using Formation.Account.Events;
using Formation.Account.Projection;
using Formation.Account.Test;
using Formation.Common.EventSourcing;
using Formation.EventStore;
using Formation.EventStore.Aggregate;
using Formation.MongoDb;

using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Internal;

using MongoDB.Driver;

namespace Formation.WebApp.Controllers
{
    [Route("api/setup")]
    public class SetupTestController : Controller
    {
        private readonly MongoRepository<TestDocument> _repository;
        private readonly EventStoreRepository _eventStoreRepository;


        public SetupTestController(MongoRepository<TestDocument> repository, EventStoreRepository eventStoreRepository)
        {
            this._repository = repository;
            this._eventStoreRepository = eventStoreRepository;
        }

        [HttpGet("self-check")]
        public async Task<ActionResult> SetupCheck()
        {
            // Test connection to event store
            await this._eventStoreRepository.AppendSingleAsync("test-setup", new TestEvent(Guid.NewGuid()));

            // Wait so projection can receive the events
            await Task.Delay(2000);

            // Test connection to MongoDB
            var documents = await this._repository.FindAllAsync();

            if (documents.Any())
            {
                return this.Ok();
            }
            else
            {
                return this.BadRequest("The cycle was not completed, be sure the Event Store have the app-all projection running.");
            }
        }
    }
}
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

using Formation.Account;
using Formation.Account.Events;
using Formation.Account.Projection;
using Formation.EventStore;
using Formation.EventStore.Aggregate;
using Formation.MongoDb;

using Microsoft.AspNetCore.Mvc;

using MongoDB.Driver;

namespace Formation.WebApp.Controllers
{
    [Route("api/EventSourcingSolutionAccounts")]
    public class EventSourcingSolutionAccountsController : Controller
    {
        private readonly MongoRepository<AccountSummaryDocument> _accountRepository;
        private readonly EventStoreRepository _eventStoreRepository;
        private readonly AggregateInvoker<AccountAggregateSolution, AccountAggregateStateSolution> _accountAggregateInvoker;


        public EventSourcingSolutionAccountsController(MongoRepository<AccountSummaryDocument> accountRepository, EventStoreRepository eventStoreRepository)
        {
            this._accountRepository = accountRepository;
            this._eventStoreRepository = eventStoreRepository;

            this._accountAggregateInvoker = new AggregateInvoker<AccountAggregateSolution, AccountAggregateStateSolution>(eventStoreRepository);
        }

        [HttpPost("create/{accountName}")]
        public async Task<ActionResult> CreateAccount([FromRoute, Required] string accountName)
        {
            // 1. Get aggregate historic events
            var accountId = HashAccountName(accountName);
            var streamName = AccountStream.GetName(accountId);
            var events = await this._eventStoreRepository.GetStreamEventsAsync(streamName, 0);

            // 2. Build aggregate state (explicit method)
            var accountState = new AccountAggregateStateSolution();

            foreach (var historicEvent in events)
            {
                switch (historicEvent.DomainEvent)
                {
                    case AccountCreatedEvent accountCreatedEvent:
                        accountState.Apply(accountCreatedEvent);
                        break;
                }
            }

            // 3. Build aggregate
            var accountAggregate = new AccountAggregateSolution(accountState);

            // 4. Execute the command
            var operationResult = accountAggregate.CreateAccount(accountId, accountName);

            if (!operationResult.IsSuccess)
            {
                return this.BadRequest(operationResult.ErrorCode);
            }

            // 5. Save new events to stream

            /* Why uncommited event instead of domain event?
            // Because uncommited event contain the expected event number,
            // This mean if an other operation was completed between the time
            // we load the events and execute the command, the event number
            // won't equal the expected events number wich mean an concurrent
            // exception occurs.
            */
            var eventsToSave = accountAggregate.UncommitedEvents;
            await this._eventStoreRepository.SaveAsync(streamName, eventsToSave);

            // 6. The command is fulfilled!

            return this.Ok(accountId);
        }

        [HttpPost("create2/{accountName}")]
        public async Task<ActionResult> CreateAccountWithEvent2([FromRoute, Required] string accountName)
        {
            // 1. Get stream name
            var accountId = HashAccountName(accountName);
            var streamName = AccountStream.GetName(accountId);

            // 2. Execute command
            var operationResult = await this._accountAggregateInvoker.InvokeAsync(streamName, agg => agg.CreateAccount(accountId, accountName));

            if (!operationResult.IsSuccess)
            {
                return this.BadRequest(operationResult.ErrorCode);
            }

            return this.Ok();
        }

        [HttpGet("{accountId}")]
        public async Task<AccountSummaryDocument> GetAccount(Guid accountId)
        {
            var filter = Builders<AccountSummaryDocument>.Filter.Eq(x => x.AccountId, accountId);

            var accountSummary = await this._accountRepository.FindOneAsync(filter);
            return accountSummary;
        }

        [HttpGet("setup-check")]
        public async Task<ActionResult> SetupCheck()
        {
            // Test connection to MongoDB
            await this._accountRepository.FindAllAsync();

            // Test connection to event store
            await this._eventStoreRepository.AppendSingleAsync("$test-setup", new AccountCreatedEvent(Guid.NewGuid(), "potato"));
            var events = await this._eventStoreRepository.GetStreamEventsAsync("$test-setup", 0);

            return this.Ok();
        }

        [HttpGet]
        public async Task<IEnumerable<AccountSummaryDocument>> GetAccounts()
        {
            var all = await this._accountRepository.FindAllAsync();
            return all;
        }

        // Little hack so the same account name generate same guid
        private static Guid HashAccountName(string accountName)
        {
            using (MD5 md5 = MD5.Create())
            {
                byte[] hash = md5.ComputeHash(Encoding.Default.GetBytes(accountName));
                return new Guid(hash);
            }
        }
    }
}
fromCategory("bank-account")
.when({
    $any: function(s,e){
        if (e.eventType === "deposit") {
            linkTo("deposits", e, e.metadata);
        }
        else if (e.eventType === "withdrawl") {
            linkTo("withdrawls", e, e.metadata);
        }
    }
})
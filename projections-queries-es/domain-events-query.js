fromAll()
.when({
    $init:function(){
        return {
            events: []
        }
    },
    $any: function(s,e){
		if(!e.eventType.startsWith("$")) {
			s.events.push(e);
		}
    }
})
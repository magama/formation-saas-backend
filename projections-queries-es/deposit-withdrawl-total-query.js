fromCategory("bank-account")
.when({
    $init:function(){
        return {
            depositTotal: 0,
            withdrawlTotal: 0
        }
    },
    $any: function(s,e){
        if (e.eventType === "deposit") {
            s.depositTotal += parseInt(e.data.amount);
        }
        else if (e.eventType === "withdrawl") {
            s.withdrawlTotal += parseInt(e.data.amount);
        }
    }
})
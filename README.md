# README #

## Setup 

For the local dev environnement we use virtual machine to host a MongoDB and a EventStore. We use vagrant to provision those VS.

Steps you need to do before starting to have fun with the backend:

0. Clone the repository  
1. Open powershell in admin and navigate into the "scripts" folder  
2. Import the module containing multiple function useful for this formation with the command `Import-Module .\Formation-Module.psm1`  
- There might be a warning message, its normal.  
3. If you don't have chocolatey installed it by running `Formation-Setup-Chocolatey`  
4. Install required dependencies for the backend application (vagrant to provision 2 VM hosting MongoDB and EventStore): `Formation-Install-Dependencies`  
- There might be an error during the virtualbox installation, ignore it.  
- Close and reopen powershell in admin, and re-import the module `Import-Module .\Formation-Module.psm1`  
5. Install [nosqlbooster](https://nosqlbooster.com/downloads) or another tool of your choice to play with MongoDB  
6. Start the services with `Formation-Start-Services`  
- If there is an error about ssh, run the command: `Formation-Reset-Services`
- You can visit "http://127.0.0.1:2113" in your browser to validate that the EventStore is up  
7. Open the solution in VS or Rider, there should be no errors  
8. For Rider users, edit your Debug configuration  
- In the Configuration tab, check "Use external console"  
- In the Browser / Live Edit tab, check "After launch" and add "http://localhost:5000/" in the field below  
9. For VS users, do not use the IIS configuration, use the Formation.WebApp configuration  
10. For all users, add the environment variable `ASPNETCORE_ENVIRONMENT` with value `Development`  
11. Start the solution and visit http://localhost:5000/, you should see a web page with no errors  
- VS sets up SSL so you can use https://localhost:5001/ instead  

## Manage local Services (MongoDB and EventStore)

* Start Services (EventStore and MongoDB): `Formation-Start-Services`
* Reset services (clear data): `Formation-Reset-Services`
* Stop Services (EventStore and MongoDB): `Formation-Stop-Services`

### FAQ

#### How can I test everything was setup correctly?

Start the application, follow the link to Swagger and use the endpoints "SetupTest/self-check"

- If 200: all good to start.
- If 400: Services are up but the cycle of an event was not completed. Do it a second time (it is timer based so super not resilient). Then check for app-all projection in event store
- Otherwise (exception): then check if EventStore and MongoDB are running (if yes try to restart them with `Formation-Reset-Services`)

#### It ask a password for vagrant, pls help:

The password is: "vagrant"

#### How can I connect to MongoDB?

We use the default connection string (no user, no security!): "mongodb://localhost:27017"

#### How can I play with EventStore?

On your browser go to "http://127.0.0.1:2113" and use the credential: (user: "admin", pwd: "changeit")

#### Why powershell in admin?

Because we don't to lose time for side effects of not being admin (some dependencies might not be installed correctly)